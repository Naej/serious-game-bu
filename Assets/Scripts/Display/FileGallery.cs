﻿using System;
using UnityEngine;

public class FileGallery : MonoBehaviour
{
    [Serializable]
    public struct FileDisplayer {
        public File file;
    }

    public FileDisplayer[] files;

    
    public int currentFileIndex;

    public FileRenderer prevFile;
    public FileRenderer currFile;
    public FileRenderer nextFile;

    

    private LockedFolder folder;

    void Start()
    {
        showFile(0);
    }

    public virtual void showFile(int index)
    {

        currentFileIndex = (index + files.Length) % files.Length;
     //   pictures[currentFileIndex].SetActive(true);
        refresh(folder);
    }

    public void nextFileClick()
    {
        showFile(currentFileIndex+1);
    }

    public FileDisplayer? getFileDisplayer(int index)
    {
        if (index < 0 || index >= files.Length)
        {
            return null;
        }
        else
        {
            return files[index];
        }
    }
    
    public void previousFileClick()
    {
        showFile(currentFileIndex-1);
    }

    public void currentFileClick()
    {
        getFileDisplayer(currentFileIndex).Value.file.setPrevious(folder);
        getFileDisplayer(currentFileIndex).Value.file.show();
    }

    public void refresh(LockedFolder folder)
    {
        this.folder = folder;
        prevFile.refresh(getFileDisplayer(currentFileIndex-1));
        currFile.refresh(getFileDisplayer(currentFileIndex));
        nextFile.refresh(getFileDisplayer(currentFileIndex+1));
    }
    
}
