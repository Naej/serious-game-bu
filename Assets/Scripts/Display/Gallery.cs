﻿using UnityEngine;

public class Gallery : MonoBehaviour
{
    public GameObject[] pictures;
    public bool looping = false;

    public int currenntPicture;

    public GameObject nextButton;
    public GameObject prevButton;

    void Start()
    {
        showPicture(0);
    }

    public virtual void showPicture(int index)
    {
        foreach (var  pict in pictures)
        {
            pict.SetActive(false);
        }

        currenntPicture = (index + pictures.Length) % pictures.Length;
        pictures[currenntPicture].SetActive(true);
        updateButtons();
    }

    public void nextPicture()
    {
        showPicture(currenntPicture+1);
    }

    public bool hasPicture(int index)
    {
        return (looping && pictures.Length > 1) || (index >= 0 && index < pictures.Length);
    }
    
    public void previousPicture()
    {
        showPicture(currenntPicture-1);
    }

    protected void updateButtons(){
        nextButton.SetActive(hasPicture(currenntPicture+1));
        prevButton.SetActive(hasPicture(currenntPicture-1));
    }
}
