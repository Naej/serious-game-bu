﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkOpener : File
{
    public string url;
    // Fake the file behaviour without closing the other files
    public override void show()
    {
        Application.OpenURL(url);
    }
}
