﻿using UnityEngine;
using UnityEngine.UI;

public class FileRenderer : MonoBehaviour
{
    private bool isActiveFile;
    public GameObject lockIcon;

    public void refresh(FileGallery.FileDisplayer? currFile)
    {
        if (!currFile.HasValue)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
            GameObject file = currFile.Value.file.gameObject;
            GetComponentInChildren<Text>().text = file.name;
            if (file.GetComponent<LockedFile>())
            {
                if (file.GetComponent<LockedFile>().isLocked())
                {
                    lockIcon.SetActive(true);
                }
                else
                {
                    lockIcon.SetActive(false);
                }
            }
            else if (file.GetComponent<File>())
            {
                lockIcon.SetActive(false);
            }
        }
    }
}
