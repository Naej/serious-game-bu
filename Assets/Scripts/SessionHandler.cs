﻿using System;
using UnityEngine;

public class SessionHandler : MonoBehaviour
{
    // Start is called before the first frame update

    public const double SESSION_LENGTH = 5;

    private const string SESSION_KEY = "session", SESSION_START_KEY = "sessionStart";
    public void Start()
    {
        if (getSession() != "")
        {
            Debug.Log(sessionTimeLeft());
            if (sessionTimeLeft() < 0)
            {
                Debug.Log("Session expired");
                PlayerPrefs.DeleteKey(SESSION_KEY);
                PlayerPrefs.DeleteKey(SESSION_START_KEY);
                PlayerPrefs.Save();
                // Tell the user to load a new session
            }
        }
        else
        {
            createSession("test");
        }
    }

    public string getSession()
    {
        return PlayerPrefs.GetString(SESSION_KEY,"");
    }

    public double sessionTimeLeft()
    {
        var date = PlayerPrefs.GetString(SESSION_START_KEY);
        var timeLeft = DateTime.Now - DateTime.Parse(date);
        return SESSION_LENGTH - timeLeft.TotalMinutes;
    }

    public void createSession(string session)
    {
        PlayerPrefs.SetString(SESSION_KEY, session);
        PlayerPrefs.SetString(SESSION_START_KEY,DateTime.Now.ToString());
        PlayerPrefs.Save();
    }
    
}
