﻿using System;
using UnityEngine.UI;

public class LoginLock : Lock
{
    public string login;
    public string password;
    
    public InputField loginField;
    public InputField passwordField;

    public void attemptLogin()
    {
        if (String.Equals(passwordField.text, password, StringComparison.CurrentCultureIgnoreCase) && 
            String.Equals(loginField.text, login, StringComparison.CurrentCultureIgnoreCase))
        {
            unlock();
        }
        else
        {
            failAttempt();
        }
    }
    
    public override void setFile(LockedFile file)
    {
        base.setFile(file);
        loginField.characterLimit = login.Length;
        passwordField.characterLimit = password.Length;

    }

    public void failAttempt()
    {
        if (!String.Equals(loginField.text, login, StringComparison.CurrentCultureIgnoreCase))
        {
            loginField.text = "";
        }

        if (!String.Equals(passwordField.text, password, StringComparison.CurrentCultureIgnoreCase))
        {
            passwordField.text = "";    
        }
    }
}
