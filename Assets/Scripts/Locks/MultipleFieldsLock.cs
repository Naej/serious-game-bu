﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MultipleFieldsLock : Lock
{
    [Serializable]
    public struct myField {
        public string password;
        public InputField field;
    }
    public myField[] fields;

    public GameObject hint;
    

    public void attemptUnlock()
    {
        bool unlock = true;
        foreach (var currentfield in fields)
        {
            unlock = unlock & checkAndUpdateField(currentfield);
        }

        if (unlock)
        {
            this.unlock();
        }
        else
        {
            showHint();
        }
    }

    private bool checkAndUpdateField(myField field)
    {
        if (field.field.text == field.password)
        {
            return true;
        }
        else
        {
            field.field.text = "";
            return false;
        }
    }

    private void showHint()
    {
        if (hint != null)
        {
            hint.SetActive(true);
        }
    }
}
