using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : File
{
    protected LockedFile file;

    public virtual void setFile(LockedFile file)
    {
        this.file = file;
        setPrevious(file.getPrevious());
    }

    public virtual void unlock()
    {
        file.unlock();
        file.show();
        Destroy(gameObject);
        
    }

    public virtual void quit()
    {
        file.showPrevious();
    }
    
}
