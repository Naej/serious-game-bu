﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TextLock : Lock
{
    public string password;
    public string hint;
    protected int attempts = 0;
    public int ATTEMPTS_BEFORE_HINT = 2;
    public Text hintField;
    public void testUnlock()
    {
        if (GetComponentInChildren<InputField>().text == password)
        {
            unlock();
        }
        else
        {
            attempts++;
            failAttempt();
            if (attempts >= ATTEMPTS_BEFORE_HINT && hintField != null)
            {
                showHint();
            }
        }
    }
    public override void setFile(LockedFile file)
    {
        base.setFile(file);
        if (string.IsNullOrEmpty(password))
        {
            Debug.LogWarning("You should not use the password nor the hint of locked file they are deprecated.");
            password = file.password;
            hint = file.hint;
        }
        GetComponentInChildren<InputField>().characterLimit = password.Length;
    }
    public void onEditText()
    {
        GetComponentInChildren<InputField>().text = GetComponentInChildren<InputField>().text.ToUpper();
    }

    public void showHint()
    {
        if (!string.IsNullOrEmpty(hint))
        {
            hintField.text = "Hint : " + hint;
        }
    }
    
    public virtual void failAttempt()
    {
        GetComponentInChildren<InputField>().text = "";
        
    }
}
