﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSelector : MonoBehaviour
{
    public enum Language{FR,EN}

    public Language currentLanguage = Language.FR;
    // Start is called before the first frame update
    void Start()
    {
        SetLanguage(currentLanguage);
    }

    public void SetLanguage(Language ln)
    {
        currentLanguage = ln;
        PlayerPrefs.SetString("Language",currentLanguage.ToString());
        PlayerPrefs.Save(); //useless in our case
    }
}
