﻿using UnityEngine;

public class File : MonoBehaviour
{
    protected File previousFile = null;
    public virtual void show()
    {

        foreach (var file in FindObjectsOfType<File>())
        {
            file.hide();
        }
        gameObject.SetActive(true);
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    public virtual void hide()
    {
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    public void setPrevious(File previous)
    {
        previousFile = previous;
    }
    
    public File getPrevious()
    {
        return previousFile;
    }

    public void showPrevious()
    {
        if (previousFile != null)
        {
            previousFile.show();
        }
        else
        {
            Application.Quit();
        }
    }
}
