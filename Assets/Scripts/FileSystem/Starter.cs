﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starter : MonoBehaviour
{
    public GameObject firstFile;
    // Start is called before the first frame update
    void Start()
    {
        firstFile.SetActive(true);
        firstFile.GetComponent<File>().show();
    }
}
