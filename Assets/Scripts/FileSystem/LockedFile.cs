﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LockedFile : File
{
    public bool _locked = true;
    public GameObject myLock;
    public string password = "TEST";
    public string hint;
    public override void show()
    {
        gameObject.SetActive(true);
        if (_locked == false)
        {
            base.show();
        }
        else
        {
            showLock();
        }
    }

    public virtual void showLock()
    {
        //check if the object is a prefab
        if (myLock.scene.name == null)
        {
            Debug.LogWarning("You should always use a custom instance of a lock, not a prefab: this is deprecated");
            myLock = Instantiate(myLock,transform.parent);
        }
        
        myLock.GetComponent<Lock>().show();
        myLock.GetComponent<Lock>().setFile(this);
        
    }

    public bool isLocked()
    {
        return _locked;
    }

    public void unlock()
    {
        _locked = false;
    }
}
