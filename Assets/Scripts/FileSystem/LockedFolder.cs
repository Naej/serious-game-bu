﻿using System;
using UnityEngine;

public class LockedFolder : LockedFile
{
    public override void show()
    {
        base.show();
        try
        {
            GetComponentInChildren<FileGallery>().refresh(this);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
        }
        

    }
}
